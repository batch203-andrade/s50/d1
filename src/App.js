import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import Courses from "./pages/Courses";

function App() {
  return (
    <div className="App">
      <AppNavbar />
      <Container>
        <Home />
        <Courses />
      </Container>
    </div>
  );
}

export default App;
