import { Row, Col } from "react-bootstrap";
import Button from '@mui/material/Button';

export default function Banner() {
    return (
        <Row>
            <Col className="p-5 text-center">
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere</p>
                <Button variant="outlined" color="primary">Enroll Now!</Button>
            </Col>
        </Row>
    )
}
